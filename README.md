# IMPRS SusMet Workshop on Research Data Management 
The three example Jupyter notebooks cover the basics of pyiron. You can access them using mybinder: 
[![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fpyiron%2Fsusmet-rdm-workshop/HEAD) 
